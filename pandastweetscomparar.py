import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt 
import seaborn as sns

usuario_de_twitter = None

def elegir_usuario_twitter(nuevo_usuario):
    global usuario_de_twitter
    usuario_de_twitter = nuevo_usuario

def datos_fecha_tweets():

    datos1 = pd.read_csv(usuario_de_twitter + '.csv')

    df1 = pd.DataFrame(datos1)
    df1['usuario1'] = df1['usuario_tweet']
    solo_fecha = pd.to_datetime(df1['fecha_retweet']).dt.date
    df1['fecha_inicio'] = solo_fecha


    df_final = df1.groupby('fecha_inicio')['usuario1'].count()
    df_final.plot(kind='line', legend = True, subplots = True)
    plt.xticks(rotation=45)
    plt.savefig(str(usuario_de_twitter)+" tweets fecha.png",bbox_inches = "tight", dpi = 200)
    plt.clf()
    return(df_final)

def datos_cantidad_tweets():

    datos1 = pd.read_csv(usuario_de_twitter + '.csv')

    df1 = pd.DataFrame(datos1)
    df_final = df1['id_tweet'].value_counts()
    df_final.plot(kind='bar', legend = True, subplots = True)
    plt.savefig(str(usuario_de_twitter)+" tweets.png",bbox_inches = "tight", dpi = 200)
    plt.clf()
    return(df_final)

def datos_usuario_cantidad_seguidores():
    df = pd.read_csv(usuario_de_twitter + '.csv')
    usuario= df.drop_duplicates('usuario_retweet')
    tabla_final = usuario[['usuario_retweet','followers']].sort_values(by = 'followers',ascending=[False]).head(15)
    tabla_final_grupo = tabla_final.groupby('usuario_retweet')['followers'].sum().sort_values(ascending=[False])
    tabla_final_grupo.plot(kind='bar', legend = True, subplots = True)
    plt.savefig(str(usuario_de_twitter)+" seguidores.png",bbox_inches = "tight", dpi = 200)
    plt.clf()
    return(tabla_final_grupo)

def datos_conteo_usuario_retweet():
    df = pd.read_csv(usuario_de_twitter + '.csv')
    usuario= df['usuario_retweet'].value_counts().head(15)
    usuario.plot(kind='bar', legend = True, subplots = True)
    plt.savefig(str(usuario_de_twitter)+" retweets.png",bbox_inches = "tight", dpi = 200)
    plt.clf()
    return usuario
