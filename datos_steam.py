from tkinter.font import names
import requests
import csv
from datetime import datetime
import ventana_Descarga as vd
import string
global usuario
usuario = None
cursor1 = "*"

def obtener_url(usuario):
    __name__ == '__main__'
    url = 'https://store.steampowered.com/appreviews/{}'.format(usuario) 
    return url

def obtener_args(cursor1):
    args ={
        'json':'1',
        'filter':'recent',
        'language':'all',
        'cursor':cursor1,
        'review_type':'all',
        'purchase_type':'all',
        #'day_range':'50000',
        'num_per_page':'100'
    }
    return(args)


def unix_a_fecha(fecha_unix):
    ts = int(str(fecha_unix))
    return(datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d'))


def strip_undesired_chars(tweet):
    stripped_tweet = tweet.replace('\n', ' ').replace('\r', '')
    char_list = [stripped_tweet[j] for j in range(len(stripped_tweet)) if ord(stripped_tweet[j]) in range(65536)]
    stripped_tweet=''
    for j in char_list:
        stripped_tweet=stripped_tweet+j
    return stripped_tweet

def direccion(usuario):
    url_final = 'https://store.steampowered.com/appreviews/{}?json=1&num_per_page=100&cursor=AoIIPxPA9HWY4/cC&language=all&filter=all&purchase_type=steam&day_range30'.format(str(usuario))
    return(url_final)


def descargar_steam(usuario):
    names =[]
    listacursor = []
    indice = 1
    global cursor1
    cursor2 = cursor1
    while(listacursor.count(cursor2)==0):
        #print(cursor1)
        #print(cursor2)
        response = requests.get(obtener_url(usuario),obtener_args(cursor2))
        if response.status_code == 200:
            payload = response.json()
            results = payload.get('reviews', [])
            listacursor.append(cursor2)
            #print(listacursor)
            cursor2 = payload.get('cursor',[])
            #print(results2)

            

        if results:
            for game in results:
                name = [(indice,unix_a_fecha(game['timestamp_created']),usuario,game['recommendationid'],game['author']['steamid'],game['received_for_free'],game['written_during_early_access'],game['voted_up'],game['language'],strip_undesired_chars(game['review']))]
                names = names+name
                indice = indice+1
                #print(names)
    return(names) 

def obtener_url_nombre(usuario):
    __name__ == '__main__'
    url = 'https://store.steampowered.com/api/appdetails?appids={}'.format(usuario) 
    return url



def obtener_nombre(usuario):
    response = requests.get(obtener_url_nombre(usuario))
    if response.status_code == 200:
            payload = response.json()
            #print(payload)
            results = payload.get(usuario,[])
            results_data = results.get('data')
            results_nombre = results_data.get('name')
            punct = string.punctuation
            for n in punct:
                results_nombre = results_nombre.replace(n, " ")
            return (results_nombre) 
  
def obtener_nombre_completo(usuario):
    nombre_final= str(usuario)+'-'+str(obtener_nombre(usuario))
    return (str(nombre_final))


def exportar_csv_steam(usuario):
    #print(usuario)
    with open(str (obtener_nombre_completo(usuario)) + '.csv', "w", newline='', encoding = "utf-8") as f:       
            writer = csv.writer(f, quoting=csv.QUOTE_ALL)
            writer.writerow(['indice','fecha_review','id_usuario_review','id_recomendacion','id_autor','juego_gratis','escrito_durante_acceso_anticipado','voto','idioma','review'])
            datos = descargar_steam(usuario)
            writer.writerows(datos)
