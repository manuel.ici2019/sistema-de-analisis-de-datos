import builtins
from tkinter import *
from tkinter.font import Font
from PIL import ImageTk, Image
import datostweet as dt 
import datos_steam as ds
import ventana_analisis_steam as vas
import pandas_review as pr
import datospublisher as dp

ventana = None
ventana_principal = None
usuario_twitter = None
codigo_steam = None
usuario_publisher = None

def descargar_datos_twitter():
    dt.exportar_csv(usuario_twitter.get())

def descargar_datos_publisher():
    dp.exportar_csv_publisher(usuario_publisher.get())

def descargar_datos_steam():
    global codigo_steam
    codigo = codigo_steam.get()
    vas.agregaropciones(ds.obtener_nombre_completo(str(codigo)))
    pr.elegir_usuario(str(codigo))
    ds.obtener_url(str(codigo))
    ds.obtener_args('*')
    ds.exportar_csv_steam(str(codigo))
    

'''mi_imagen = Image.open("iconos/flecha2.png")
escalar = mi_imagen.resize((50,50), Image.ANTIALIAS)
mi_imagen2 = ImageTk.PhotoImage(escalar)'''
'''flecha_retroceso = Button(ventana_descarga, image = mi_imagen, bg ='blue')
    flecha_retroceso.place(relx=0.025, rely=0.025)'''

def cerrar_ventana_descarga():
    global ventana_principal
    ventana_principal.deiconify()
    ventana.destroy()





def ventana_Descargar(ventana_original):
    global ventana_principal
    ventana_principal = ventana_original
    global ventana
    ventana = Tk()
    ventana.title = "SAD"
    ventana.geometry = "600x300+500+500"

   


    ventana_descarga = Frame(ventana, bg = "blue")
    ventana_descarga.pack(expand = True, fill = "both")

    etiqueta_titulo = Label(ventana_descarga, text = "Nombre Usuario Twitter", bg = 'blue', font = (('Arial'),15), fg = 'white')
    etiqueta_titulo.grid(row = 1, column = 1, padx = 10)
    etiqueta_espacio = Label(ventana_descarga, bg = 'blue')
    etiqueta_espacio.grid(row = 3, column = 1, padx = 10)
    etiqueta_titulo_publisher = Label(ventana_descarga, text = "Nombre Usuario Publisher", bg = 'blue', font = (('Arial'),15), fg = 'white')
    etiqueta_titulo_publisher.grid(row = 4, column = 1, padx = 10)
    etiqueta_titulo_Steam = Label(ventana_descarga, text = "Codigo Videojuego Steam",bg = 'blue', font = (('Arial'),15), fg = 'white')
    etiqueta_titulo_Steam.grid(row = 1, column = 2, padx = 10)

    global usuario_twitter 
    usuario_twitter = Entry(ventana_descarga, bg = "white", width = 30, font = 'Arial 15')
    usuario_twitter.grid(row = 2, column = 1, pady = 5, padx = 10)

    global usuario_publisher
    usuario_publisher = Entry(ventana_descarga, bg = "white", width = 30, font = 'Arial 15')
    usuario_publisher.grid(row = 5, column = 1, pady = 5, padx = 10)

    global codigo_steam
    codigo_steam = Entry(ventana_descarga, bg = "white", width = 30, font = 'Arial 15')
    codigo_steam.grid(row = 2, column = 2, pady = 5, padx = 10)

    Boton_descarga = Button(ventana_descarga, text = "Descargar Twitter", height = 2, command = descargar_datos_twitter)
    Boton_descarga.grid(row = 3, column = 1, pady = 5, sticky= W+E, padx =10)

    Boton_descarga = Button(ventana_descarga, text = "Descarga Publisher", height = 2, command = descargar_datos_publisher)
    Boton_descarga.grid(row = 6, column = 1, pady = 5, sticky= W+E, padx =10)

    Boton_descarga_steam = Button(ventana_descarga, text = "Descargar Steam", height = 2, command = descargar_datos_steam)
    Boton_descarga_steam.grid(row = 3, column = 2, pady = 5, sticky= W+E, padx =10)

    boton_retroceso = Button(ventana_descarga, text = 'Retroceder', bg = 'green', command = cerrar_ventana_descarga )
    boton_retroceso.grid(row = 1, column = 0, pady = 10, padx = 5)
    
    



    ventana.mainloop()
