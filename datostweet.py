import tweepy
import csv
import credenciales
import json
import urllib
import re 
from re import split


#Autenticar llaves
auth = tweepy.OAuthHandler(credenciales.API_KEY,credenciales.API_SECRET_KEY)
auth.set_access_token(credenciales.ACCESS_TOKEN,credenciales.ACCESS_TOKEN_SECRET)

#ingresar a la API
api = tweepy.API(auth)


# fetching the statuses
def tweets_historicos(nombre_de_usuario):
    nombre = nombre_de_usuario
    statuses = api.user_timeline(screen_name = nombre, exclude_replies = True, include_rts = False, count = 100)
    id = []
    for s in statuses:
    # print (json.dumps(s._json, indent=2))
        id.append(s.id)
    return (id)

def retweet_historico(nombre_de_usuario):
    nombre = nombre_de_usuario
    IDs = tweets_historicos(nombre)
    #print(IDs)
    todos_los_tweets = []
    salida_csv =[]
    indice = 1
    quiebre = 1
    for id in IDs:
        todos_los_tweets = api.get_retweets(id, count = 100)
        for tweet in todos_los_tweets:
            salida_tweets = [(indice,tweet.retweeted_status.created_at,(tweet.entities['user_mentions'])[0]['id'],(tweet.entities['user_mentions'])[0]['screen_name'],id,tweet.created_at, tweet.user.id_str, tweet.user.screen_name,tweet.user.created_at, strip_undesired_chars(tweet.user.location), tweet.user.followers_count, tweet.user.friends_count) ] 
            indice = indice+1
            salida_csv = salida_csv + salida_tweets
        quiebre = quiebre+1
        if quiebre == len(tweets_historicos(nombre)):
            break
    return salida_csv

        # printing the screen names of the retweeters
        
def strip_undesired_chars(tweet):
    stripped_tweet = tweet.replace('\n', ' ').replace('\r', '')
    char_list = [stripped_tweet[j] for j in range(len(stripped_tweet)) if ord(stripped_tweet[j]) in range(65536)]
    stripped_tweet=''
    for j in char_list:
        stripped_tweet=stripped_tweet+j
    return stripped_tweet


    
def exportar_csv(nombre_de_usuario):
    nombre = nombre_de_usuario
    with open(nombre + '.csv', "w", newline='', encoding = "utf-8") as f:       
                writer = csv.writer(f, quoting=csv.QUOTE_ALL)
                writer.writerow(['indice','fecha_tweet','id_usuario_tweet','usuario_tweet','id_tweet','fecha_retweet','id_usuario_retweet','usuario_retweet','fecha_creacion_cuenta','localizacion','followers', 'friends'])
                writer.writerows(retweet_historico(nombre))    
                




#print (retweet_historico())
#print (crear_lista_csv())
#exportar_csv('@DualEffectGames')
  
    

    
    



    

