from io import UnsupportedOperation
from matplotlib.transforms import Bbox
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt 
import seaborn as sns
import numpy as np
from operator import itemgetter, sub
import requests
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from tkinter import*

global results

usuario100 = None
usuario200 = None
#usuario100 = "Tormented Souls"
#usuario200 = 'Tormented Souls'

def cambio_usuario(usuario_elegido):
    global usuario100
    usuario100 = usuario_elegido
    return(usuario100)

def formater_usuario(usuario):
    numero = usuario.find("-")
    usuario_formato=(usuario[:numero])
    return(usuario_formato)

def usuario_nombre():
    nombre_usuario = usuario100
    return(nombre_usuario)

def usuario_nombre_formateado():
    numero = usuario_nombre().find("-")
    usuario_formato=(usuario_nombre()[numero+1:])
    return(usuario_formato)


def usuario_nombre2():
    nombre_usuario = usuario200
    return(nombre_usuario)

def usuario_nombre_formateado2():
    numero = usuario_nombre2().find("-")
    usuario_formato=(usuario_nombre2()[numero+1:])
    return(usuario_formato)

def llamada_API():
    __name__ == '__main__'
    url = 'https://store.steampowered.com/api/appdetails?appids={}'.format(formater_usuario(usuario100))
    response = requests.get(url)
    if response.status_code == 200:
        payload = response.json()
        results = payload.get(formater_usuario(usuario100), [])
        return(results)
def datos_grafico(results):
    
            #print(results)

            if results:
                contar=0
                fecha_lanzamiento=''
                for fecha in results['data']['release_date']['date']:
                    if contar > 7:
                        fecha_lanzamiento = fecha_lanzamiento + fecha
                    contar = contar+1
                fecha_lanzamiento = int(fecha_lanzamiento)
                nbnumber = 0
                if fecha_lanzamiento <= 2017:
                    nbnumber= 81
                elif fecha_lanzamiento == 2018:
                    nbnumber = 82
                elif fecha_lanzamiento == 2019:
                    nbnumber = 57
                elif fecha_lanzamiento >=2020:
                    nbnumber= 41 
                datos = pd.read_csv(usuario100+'.csv')

                df = pd.DataFrame(datos)

                data_original = (df['fecha_review'].value_counts().to_dict())

                name = sorted(data_original.items())
                fecha =[]
                valor = []
                for n in name:
                    fecha.append(n[0])
                    valor.append(n[1])

                df1 = {'fecha':(fecha),'valor' :(valor)}

                df1 = pd.DataFrame(df1, columns=['fecha','valor'])
                df1['valor_acumulado'] = df1.valor.cumsum()
                df1['cantidad_ventas_acumuladas'] = df1.valor.cumsum()*nbnumber

                #df1['fecha']
                #df1.plot(kind='line', legend='Reverse')
                #df.groupby('cantidad_voto').cumsum().plot(kind='line', legend='Reverse')
                #df1.groupby('fecha')['valor_acumulado'].sum().plot(kind='line', legend='Reverse')


                d2 = df1.groupby('fecha')['cantidad_ventas_acumuladas'].sum()
                d2.plot(kind='line', legend = True, subplots = True, color = 'blue')
                plt.xticks(rotation=45)
                plt.savefig(str(usuario_nombre_formateado())+" ventas.png", bbox_inches = "tight", dpi = 200)
                plt.clf()
                #print(d2)
                return (d2)

    #nbnumber2017= 81
    #nbnumber2018 = 82
    #nbnumber2019 = 57
    #nbnumber2020 = 41
def datos_steam2():
    datos = pd.read_csv(usuario100+'.csv')
    df = pd.DataFrame(datos)
    d2 = df.groupby('fecha_review')['id_usuario_review'].count()
    d2.plot(kind='line', legend = True, subplots = True, color = 'blue')
    plt.xticks(rotation=45)
    plt.savefig(str(usuario_nombre_formateado())+" review.png",bbox_inches = "tight", dpi = 200)
    plt.clf()
    #.plot(kind='line', legend='Reverse')
    return (d2)

#cambio_usuario(1234)
#print(llamada_API())

def cambio_usuario2(usuario_elegido):
    global usuario200
    usuario200 = usuario_elegido
    return(usuario200)

def llamada_API2():
    __name__ == '__main__'
    url = 'https://store.steampowered.com/api/appdetails?appids={}'.format(formater_usuario(usuario200))
    response = requests.get(url)
    if response.status_code == 200:
        payload = response.json()
        results = payload.get(usuario200, [])
        return(results)

def datos_grafico2(results):
    
            #print(results)

            if results:
                contar=0
                fecha_lanzamiento=''
                for fecha in results['data']['release_date']['date']:
                    if contar > 7:
                        fecha_lanzamiento = fecha_lanzamiento + fecha
                    contar = contar+1
                fecha_lanzamiento = int(fecha_lanzamiento)
                nbnumber = 0
                if fecha_lanzamiento <= 2017:
                    nbnumber= 81
                elif fecha_lanzamiento == 2018:
                    nbnumber = 82
                elif fecha_lanzamiento == 2019:
                    nbnumber = 57
                elif fecha_lanzamiento >=2020:
                    nbnumber= 41 
                datos = pd.read_csv(usuario200+'.csv')

                df = pd.DataFrame(datos)

                data_original = (df['fecha_review'].value_counts().to_dict())

                name = sorted(data_original.items())
                fecha =[]
                valor = []
                for n in name:
                    fecha.append(n[0])
                    valor.append(n[1])

                df1 = {'fecha':(fecha),'valor' :(valor)}

                df1 = pd.DataFrame(df1, columns=['fecha','valor'])
                df1['valor_acumulado'] = df1.valor.cumsum()
                df1['cantidad_ventas_acumuladas'] = df1.valor.cumsum()*nbnumber

                #df1['fecha']
                #df1.plot(kind='line', legend='Reverse')
                #df.groupby('cantidad_voto').cumsum().plot(kind='line', legend='Reverse')
                #df1.groupby('fecha')['valor_acumulado'].sum().plot(kind='line', legend='Reverse')


                d2 = df1.groupby('fecha')['cantidad_ventas_acumuladas'].sum()
                d2.plot(kind='line', legend = True, subplots = True, color = 'blue')
                plt.xticks(rotation=45)
                plt.savefig(str(usuario_nombre_formateado2())+" ventas.png", bbox_inches = "tight", dpi = 200)
                plt.clf()
                #print(d2)
                return (d2)

def datos_steam22():
    datos = pd.read_csv(usuario200+'.csv')
    df = pd.DataFrame(datos)
    d2 = df.groupby('fecha_review')['id_usuario_review'].count()
    #.plot(kind='line', legend='Reverse')
    d2.plot(kind='line', legend = True, subplots = True, color = 'blue')
    plt.xticks(rotation=45)
    plt.savefig(str(usuario_nombre_formateado2())+" review.png",bbox_inches = "tight", dpi = 200)
    plt.clf()
    return (d2)

def datos_steam_negativos():
    datos = pd.read_csv(usuario100+'.csv')
    df = pd.DataFrame(datos)
    #print (df)
    indexNames = df[ df["voto"] ==True].index
    df.drop(indexNames, inplace = True)
    d2=(df)
    d2 = d2.groupby('fecha_review')['id_usuario_review'].count()
    d2.plot(kind='line', legend = True, subplots = True, color = 'red')
    plt.xticks(rotation=45)
    plt.savefig(str(usuario_nombre_formateado())+" negativo.png",bbox_inches = "tight", dpi = 200)
    plt.clf()
    #print(d2)
    return (d2)

def datos_steam_negativos2():
    datos = pd.read_csv(usuario200+'.csv')
    df = pd.DataFrame(datos)
    #print (df)
    indexNames = df[ df["voto"] ==True].index
    df.drop(indexNames, inplace = True)
    d2=(df)
    d2 = d2.groupby('fecha_review')['id_usuario_review'].count()
    d2.plot(kind='line', legend = True, subplots = True, color = 'red')
    plt.xticks(rotation=45)
    plt.savefig(str(usuario_nombre_formateado2())+" negativo.png",bbox_inches = "tight", dpi = 200)
    plt.clf()
    return (d2)

