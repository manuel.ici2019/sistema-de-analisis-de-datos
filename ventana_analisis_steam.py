from tkinter import *
from tkinter.font import Font
from PIL import ImageTk, Image
from tkinter import ttk
import ventana_reporte_steam as vr
import ventana_reporte_steam_comparar as vrc
import pandas_review_acumulado as pra
import pandas_review as pr

ventana = None
ventana_principal = None
opciones = []

def agregaropciones(nueva_opcion):
    global opciones
    opciones.append(nueva_opcion)


def cerrar_ventana_analisis():
    global ventana_principal
    ventana_principal.deiconify()
    ventana.destroy()


def abrir_ventana_reporte(usuario_twitter):
    global ventana_principal
    #print(usuario_twitter.get())
    pr.elegir_usuario(str(usuario_twitter.get()))
    pra.cambio_usuario (str(usuario_twitter.get()))
    ventana.withdraw()
    vr.ventana_reporte(ventana)

def abrir_ventana_reporte_comparar(usuario_comparar1, usuario_comparar2):
    global ventana_principal
    pr.elegir_usuario(usuario_comparar1.get())
    pr.elegir_usuario2(usuario_comparar2.get())
    pra.cambio_usuario (usuario_comparar1.get())
    pra.cambio_usuario2 (usuario_comparar2.get())
    ventana.withdraw()
    vrc.ventana_reporte(ventana)
   
    
    


def ventana_analisis(ventana_original):

    global ventana_principal
    ventana_principal = ventana_original
    global ventana
    ventana = Tk()
    ventana.title = "SAD"
    ventana.geometry = "600x300+500+500"


    

    F_individual = Frame(ventana, bg = "blue" )
    F_individual.pack(expand = True , fill = 'both')

    boton_retroceso = Button(F_individual, text = 'retroceder', bg = 'green', command = cerrar_ventana_analisis)
    boton_retroceso.grid( row = 0, column = 0, pady = 5, sticky = W, padx = 5)

    etiqueta_titulo = Label(F_individual, text = "Codigo Usuario Steam", font = 'Arial 12', fg = 'white', bg = 'blue')
    etiqueta_titulo.grid(row = 1, column = 2, padx = 70, pady = 5)
    
    opciones
    usuario_twitter = StringVar()
    usuario_twitter_combobox = ttk.Combobox(F_individual, textvariable = usuario_twitter, values = opciones, width=30)
    usuario_twitter_combobox.grid(row = 2, column = 2,padx = 70, pady = 5)

    Boton_individual = Button(F_individual, text = 'Analizar', bg = "white", command = lambda: abrir_ventana_reporte(usuario_twitter_combobox))
    Boton_individual.grid(row = 3, column = 2, padx = 70, pady = 20)


    F_comparativo = Frame(ventana, bg = "red" )
    F_comparativo.pack(expand = True , fill = 'both')

    etiqueta_comparar1 = Label(F_comparativo, text = "Nombre Usuario 1", font = 'Arial 12', bg = 'red', fg = 'white')
    etiqueta_comparar1.grid(row = 0, column = 0, padx = 20, pady = 5)

    usuario_comparar1 = StringVar()
    usuario_comparar1_combobox = ttk.Combobox(F_comparativo, textvariable = usuario_comparar1, values = opciones, width=30)
    usuario_comparar1_combobox.grid(row = 2, column = 0,padx = 20, pady = 5)
    

    etiqueta_comparar2 = Label(F_comparativo, text = "Nombre Usuario 2", font = 'Arial 12', bg = 'red', fg = 'white')
    etiqueta_comparar2.grid(row = 0, column = 1, padx = 20, pady = 5 )

    usuario_comparar2 = StringVar()
    usuario_comparar2_combobox = ttk.Combobox(F_comparativo, textvariable = usuario_comparar2, values = (opciones), width=30)
    usuario_comparar2_combobox.grid(row = 2, column = 1,padx = 20, pady = 5)
    


    Boton_comparar = Button(F_comparativo, text = 'Comparar', bg = "white", fg = 'blue',command = lambda: abrir_ventana_reporte_comparar(usuario_comparar1_combobox,usuario_comparar2_combobox))
    Boton_comparar.place(x =210, y = 80)
    blanco = Label(F_comparativo, text = "", bg = 'red', height=4)
    blanco.grid(row=3, column =0)



    ventana.mainloop()