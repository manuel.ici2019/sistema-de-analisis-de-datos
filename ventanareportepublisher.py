from tkinter import *
from tkinter import ttk
import pandastweets as pt
from tkinter.font import BOLD
import pandastweetscomparar as ptc
import matplotlib.pyplot as plt 
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from fpdf import FPDF
import pandaspublisher as pp

ventana = None
ventana_principal = None

def cerrar_ventana_reporte():
    global ventana_principal
    ventana_principal.deiconify()
    ventana.destroy()

def guardarpdfpublisher():
    pdf = FPDF(orientation='P', unit = 'mm', format = 'A4')
    pdf.add_page()
    pdf.set_font("Arial",'B',20)
    pdf.text(x = 60, y = 30, txt = 'Análisis '+ str(pp.usuario_publisher))
    pdf.set_font("Arial",'',16)
    pdf.text(x = 10, y = (60), txt = 'Videojuego Buscado: '+ str(pp.busqueda_videojuego))
    pdf.text(x = 10, y = (70), txt = '')
    pdf.text(x = 10, y = (80), txt = 'fecha inicio: '+ str(pp.fecha_fin()))
    pdf.text(x = 10, y = (90), txt = 'fecha Termino: '+ str(pp.fecha_inicio()))
    pdf.text(x = 10, y = (100), txt = '')
    pdf.text(x = 10, y = (110), txt = 'Cantidad de menciones: '+ str(pp.cantidaddemenciones(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda()))))
    pdf.text(x = 10, y = (120), txt = '')
    pdf.text(x = 10, y = (130), txt = 'ID Menciones')
    lineas = 140
    lineas2 = 140
    lista=[]
    indice = 0
    conteo=0
    for d in pp.id_menciones(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda())):
        lista.append(d)
        #print (lista)
        if (lineas == 280):    
            pdf.add_page()
            pdf.text(x = 60, y = 30, txt = 'Análisis '+ str(pp.usuario_publisher))
            pdf.text(x = 10, y = (50), txt = 'ID Menciones')
            lineas = 60
            
        else:    
            pdf.text(x = 10, y = (lineas), txt = str(d))
            #print(lineas)
            #print(d)
            lineas = lineas +10
    lineas2 = lineas
    lista=[]
    indice = 0
    conteo=0
    if (lineas == 280):    
            pdf.add_page()
            pdf.text(x = 60, y = 30, txt = 'Análisis '+ str(pp.usuario_publisher))
            pdf.text(x = 10, y = (50), txt = 'Menciones por Fecha')
            lineas = 60
    pdf.text(x = 10, y = lineas+10, txt = 'Menciones por Fecha')
    lineas = lineas+20
    lineas2 = lineas
    for d in pp.conteo_fecha(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda())):
        lista.append(d)
        #print (lista)
        if (lineas == 280):
            for d2 in pp.conteo_fecha(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda())):
                conteo = conteo +1
                if lineas2 ==280:
                    pdf.text(x = 100, y = (lineas2), txt = str(pp.conteo_fecha(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda()))[(lista[indice])]))
                    pdf.text(x = 10, y = (lineas), txt = str(d))
                    lineas = lineas+10
                    #print (indice)
                    lineas2 = 60
                    lista = []
                    indice = 0
                    break
                else:
                    pdf.text(x = 100, y = (lineas2), txt = str(pp.conteo_fecha(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda()))[(lista[indice])]))
                    lineas2 = lineas2 +10
                    indice = indice + 1
                    #print (indice)
                
            pdf.add_page()
            pdf.text(x = 60, y = 30, txt = 'Análisis '+str(pp.usuario_publisher()))
            pdf.text(x = 10, y = (50), txt = 'Usuarios con mayor cantidad de seguidores')
            lineas = 60
            
        else:    
            pdf.text(x = 10, y = (lineas), txt = str(d))
            #print(lineas)
            #print(d)
            lineas = lineas +10
    #print (conteo)
    #print(len(d1))
    if conteo < len(pp.conteo_fecha(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda()))):
        for n in range(len(pp.conteo_fecha(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda())))-conteo):
            pdf.text(x = 100, y = (lineas2), txt = str(pp.conteo_fecha(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda()))[(lista[indice])]))
            lineas2 = lineas2 +10
            indice = indice + 1

    pdf.output(str(pp.usuario_publisher_sin_signo(pp.usuario_publisher))+"publisher.pdf")
    

def ventana_reporte(ventana_original):
    global ventana_principal
    ventana_principal = ventana_original
    global ventana
    ventana = Tk()
    ventana.title = 'SAD'
    ventana.geometry = '500x450'

    main_frame = Frame(ventana,bg = 'blue')
    main_frame.pack(fill = BOTH, expand = 1)

    my_canvas = Canvas(main_frame, bg = 'blue')
    my_canvas.pack(side = LEFT, fill = BOTH, expand = 1)

    my_scrollbar = ttk.Scrollbar(main_frame, orient = VERTICAL, command = my_canvas.yview)
    my_scrollbar.pack ( side = RIGHT, fill = Y)

    my_canvas.configure(yscrollcommand = my_scrollbar.set )

    my_canvas.bind('<Configure>', lambda e: my_canvas.configure(scrollregion = my_canvas.bbox('all')))

    second_frame = Frame(my_canvas, bg = 'blue')
    second_frame.pack(expand = True, fill = 'both', side = RIGHT)

    my_canvas.create_window((0,0), window = second_frame, anchor = 'nw')

    second_frame = Frame(second_frame, bg = 'red')
    second_frame.pack(expand = True, fill = 'both')

    primera_mitad = Frame(second_frame, bg = "blue")
    primera_mitad.pack( side = LEFT, fill = BOTH, expand = 1)

    segunda_mitad = Frame(second_frame, bg = "blue")
    segunda_mitad.pack(fill = BOTH, expand = 1)

    espacio = Label(primera_mitad, text ='', bg = 'blue')
    espacio.grid(row = 1, column =1, pady = 5, padx = 10, sticky = W)

    etiqueta_titulo_videojuego =Label(primera_mitad, text = 'Videojuego Buscado :', font = (('Arial'),15,BOLD))
    etiqueta_titulo_videojuego.grid(row = 2, column =0, pady = 5, padx = 10, sticky = W)
    titulo_videojuego = Entry(primera_mitad, width = 15, font='Arial 15', justify = CENTER)
    titulo_videojuego.insert(0,pp.busqueda_videojuego)
    titulo_videojuego.grid(row =2,column = 1)

    espacio = Label(primera_mitad, text ='', bg = 'blue')
    espacio.grid(row = 3, column =1, pady = 5, padx = 10, sticky = W)

    etiqueta_titulo_fecha =Label(primera_mitad, text = 'Fecha Datos', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha.grid(row = 4, column =0, pady = 5, padx = 10, sticky = W)

    etiqueta_fecha_inicio =Label(primera_mitad, text = 'Fecha inicio :', font = (('Arial'),15))
    etiqueta_fecha_inicio.grid(row = 5, column =0, pady = 5, padx = 10, sticky = W)
    fecha_inicio = Entry(primera_mitad, width = 15, font='Arial 15', justify = CENTER)
    fecha_inicio.insert(0,pp.fecha_fin())
    fecha_inicio.grid(row =5,column = 1)

    etiqueta_fecha_final =Label(primera_mitad, text = 'Fecha termino :', font = (('Arial'),15))
    etiqueta_fecha_final.grid(row = 6, column = 0, pady = 5, padx = 10, sticky = W)
    fecha_final = Entry(primera_mitad, width = 15, font='Arial 15', justify=CENTER)
    fecha_final.insert(0,pp.fecha_inicio())
    fecha_final.grid(row = 6, column = 1, padx = 10)

    boton_retroceso = Button(primera_mitad, text = 'Retroceder', bg = 'green', command = cerrar_ventana_reporte)
    boton_retroceso.grid(row = 0, column = 0, pady = 2, padx = 2, sticky = W)

    espacio = Label(primera_mitad, text ='', bg = 'blue')
    espacio.grid(row = 7, column =1, pady = 5, padx = 10, sticky = W)
    
    etiqueta_menciones=Label(primera_mitad, text = 'Cantidad de menciones', font = (('Arial'),15, BOLD))
    etiqueta_menciones.grid(row = 8, column = 0, pady = 5, padx = 10, sticky = W)
    fecha_final = Entry(primera_mitad, width = 15, font='Arial 15', justify=CENTER)
    fecha_final.insert(0,pp.cantidaddemenciones(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda())))
    fecha_final.grid(row = 8, column = 1, padx = 10)

    espacio = Label(primera_mitad, text ='', bg = 'blue')
    espacio.grid(row = 9, column =1, pady = 5, padx = 10, sticky = W)

    
    etiqueta_titulo_fecha =Label(primera_mitad, text = 'ID menciones', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha.grid(row = 10, column = 0, pady = 5, padx = 10, sticky = W)
    i = 11
    for cantidad in pp.id_menciones(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda())):
        tweets = Label(primera_mitad, text = cantidad, font = 'Arial 15')
        tweets.grid(row = i, column = 0, pady = 5, padx = 10, sticky = W)
        i = i + 1

    espacio = Label(primera_mitad, text ='', bg = 'blue')
    espacio.grid(row = i+2, column =1, pady = 5, padx = 10, sticky = W)    
    
    etiqueta_titulo_fecha_retweet = Label(primera_mitad, text = 'Menciones por Fecha', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha_retweet.grid(row = i+3, column = 0,sticky=W, pady = 10, padx = 10)
    i = i+4
    for conteo in pp.conteo_fecha(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda())):
        tweets = Label(primera_mitad, text = conteo, font = 'Arial 15')
        tweets.grid(row = i, column =0, pady = 5, padx = 20, sticky = W)
        tweets_cuenta = Label(primera_mitad, text = pp.conteo_fecha(pp.nombre_busqueda(),pp.formater_usuario_publisher(pp.nombre_busqueda()))[conteo], font = 'Arial 15')
        tweets_cuenta.grid(row = i, column =1, pady = 5, padx = 20, sticky = W)
        i = i + 1

    etiqueta_titulo_fecha =Label(primera_mitad, text = '', bg="blue")
    etiqueta_titulo_fecha.grid(row = i+1, column =0, pady = 5, padx = 10, sticky = W)

    boton_guardar = Button(primera_mitad, text = 'Guardar en PDF', bg = 'green',fg = 'white', command = guardarpdfpublisher)
    boton_guardar.grid(row = i+2, column = 0, pady = 2, padx = 2, sticky = W)


    ventana.mainloop()