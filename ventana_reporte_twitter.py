from tkinter import *
from tkinter import ttk
import pandastweets as pt
from tkinter.font import BOLD
import pandastweetscomparar as ptc
import matplotlib.pyplot as plt 
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from fpdf import FPDF

ventana = None
ventana_principal = None

def guardarpdftwitter():
    pdf = FPDF(orientation='P', unit = 'mm', format = 'A4')
    pdf.add_page()
    pdf.set_font("Arial",'B',20)
    pdf.text(x = 60, y = 30, txt = 'Análisis '+ str(pt.usuario_twitter()))
    pdf.set_font("Arial",'',16)
    pdf.text(x = 10, y = (60), txt = 'fecha inicio: '+ str(pt.fecha_fin()))
    pdf.text(x = 10, y = (70), txt = 'fecha Termino: '+ str(pt.fecha_inicio()))
    pdf.text(x = 10, y = (80), txt = 'Cantidad retweet por tweet')
    lineas = 90
    lineas2 = 90
    lista=[]
    indice = 0
    conteo=0
    for d in pt.cantidad_de_retweet_por_tweet_dict():
        lista.append(d)
        #print (lista)
        if (lineas == 280):
            for d2 in pt.cantidad_de_retweet_por_tweet_dict():
                conteo = conteo +1
                if lineas2 ==280:
                    pdf.text(x = 90, y = (lineas2), txt = str(pt.cantidad_de_retweet_por_tweet_dict()[(lista[indice])]))
                    pdf.text(x = 10, y = (lineas), txt = str(d))
                    lineas = lineas+10
                    #print (indice)
                    lineas2 = 60
                    lista = []
                    indice = 0
                    break
                else:
                    pdf.text(x = 90, y = (lineas2), txt = str(pt.cantidad_de_retweet_por_tweet_dict()[(lista[indice])]))
                    lineas2 = lineas2 +10
                    indice = indice + 1
                    #print (indice)
                
            pdf.add_page()
            pdf.text(x = 60, y = 30, txt = 'Análisis '+ str(pt.usuario_twitter()))
            pdf.text(x = 10, y = (50), txt = 'Cantidad retweet por tweet')
            lineas = 60
            
        else:    
            pdf.text(x = 10, y = (lineas), txt = str(d))
            #print(lineas)
            #print(d)
            lineas = lineas +10
    #print (conteo)
    #print(len(d1))
    if conteo < len(pt.cantidad_de_retweet_por_tweet_dict()):
        for n in range(len(pt.cantidad_de_retweet_por_tweet_dict())-conteo):
            pdf.text(x = 90, y = (lineas2), txt = str(pt.cantidad_de_retweet_por_tweet_dict()[(lista[indice])]))
            lineas2 = lineas2 +10
            indice = indice + 1
    if lineas >110:
        pdf.add_page()
        pdf.text(x = 60, y = 30, txt = 'Análisis '+ str(pt.usuario_twitter()))
        pdf.text(x = 10, y = (50), txt = 'Gráfico Cantidad retweet por tweet')
        pdf.image("@"+str(pt.usuario_twitter())+" tweets.png", x = 0, y = 60 , w = 200, h = 150)
        lineas = 60
    else:
        pdf.text(x = 10, y = lineas +20, txt = 'Gráfico Cantidad retweet por tweet')
        pdf.image("@"+str(pt.usuario_twitter())+" tweets.png", x = 0, y = lineas +30 , w = 200, h = 150)

    pdf.add_page()
    pdf.text(x = 60, y = 30, txt = 'Análisis '+ str(pt.usuario_twitter()))
    pdf.text(x = 10, y = (50), txt = 'Promedio de Retweets')
    pdf.text(x = 10, y = (60), txt = 'promedio : '+str(pt.promedio_retweet()))
    pdf.text(x = 10, y = (70), txt = 'Cantidad retweets por fecha')
    lineas = 80
    lineas2 = 80
    lista=[]
    indice = 0
    conteo=0
    for d in pt.conteo_fecha_retweets_dict():
        lista.append(d)
        #print (lista)
        if (lineas == 280):
            for d2 in pt.conteo_fecha_retweets_dict():
                conteo = conteo +1
                if lineas2 ==280:
                    pdf.text(x = 50, y = (lineas2), txt = str(pt.conteo_fecha_retweets_dict()[(lista[indice])]))
                    pdf.text(x = 10, y = (lineas), txt = str(d))
                    lineas = lineas+10
                    #print (indice)
                    lineas2 = 60
                    lista = []
                    indice = 0
                    break
                else:
                    pdf.text(x = 50, y = (lineas2), txt = str(pt.conteo_fecha_retweets_dict()[(lista[indice])]))
                    lineas2 = lineas2 +10
                    indice = indice + 1
                    #print (indice)
                
            pdf.add_page()
            pdf.text(x = 60, y = 30, txt = 'Análisis '+str(pt.usuario_twitter()))
            pdf.text(x = 10, y = (50), txt = 'Cantidad retweet por fecha')
            lineas = 60
            
        else:    
            pdf.text(x = 10, y = (lineas), txt = str(d))
            #print(lineas)
            #print(d)
            lineas = lineas +10
    #print (conteo)
    #print(len(d1))
    if conteo < len(pt.conteo_fecha_retweets_dict()):
        for n in range(len(pt.conteo_fecha_retweets_dict())-conteo):
            pdf.text(x = 50, y = (lineas2), txt = str(pt.conteo_fecha_retweets_dict()[(lista[indice])]))
            lineas2 = lineas2 +10
            indice = indice + 1
    print (lineas)
    if lineas >110:
        pdf.add_page()
        pdf.text(x = 60, y = 30, txt = 'Análisis '+str(pt.usuario_twitter()))
        pdf.text(x = 10, y = (50), txt = 'Gráfico Cantidad retweet por fecha')
        pdf.image("@"+str(pt.usuario_twitter())+" tweets fecha.png", x = 0, y = 60 , w = 200, h = 150)
        lineas = 60
    else:
        pdf.text(x = 10, y = lineas +20, txt = 'Gráfico Cantidad retweet por fecha')
        pdf.image("@"+str(pt.usuario_twitter())+" tweets fecha.png", x = 0, y = lineas +30 , w = 200, h = 150)
    
    pdf.add_page()
    pdf.text(x = 60, y = 30, txt = 'Análisis '+str(pt.usuario_twitter()))
    pdf.text(x = 10, y = (50), txt = 'Usuarios con mayor cantidad de seguidores')
    lineas = 60
    lineas2 = 60
    lista=[]
    indice = 0
    conteo=0
    for d in pt.usuario_cantidad_seguidores():
        lista.append(d)
        #print (lista)
        if (lineas == 280):
            for d2 in pt.usuario_cantidad_seguidores():
                conteo = conteo +1
                if lineas2 ==280:
                    pdf.text(x = 100, y = (lineas2), txt = str(pt.usuario_cantidad_seguidores()[(lista[indice])]))
                    pdf.text(x = 10, y = (lineas), txt = str(d))
                    lineas = lineas+10
                    #print (indice)
                    lineas2 = 60
                    lista = []
                    indice = 0
                    break
                else:
                    pdf.text(x = 100, y = (lineas2), txt = str(pt.usuario_cantidad_seguidores()[(lista[indice])]))
                    lineas2 = lineas2 +10
                    indice = indice + 1
                    #print (indice)
                
            pdf.add_page()
            pdf.text(x = 60, y = 30, txt = 'Análisis '+str(pt.usuario_twitter()))
            pdf.text(x = 10, y = (50), txt = 'Usuarios con mayor cantidad de seguidores')
            lineas = 60
            
        else:    
            pdf.text(x = 10, y = (lineas), txt = str(d))
            #print(lineas)
            #print(d)
            lineas = lineas +10
    #print (conteo)
    #print(len(d1))
    if conteo < len(pt.usuario_cantidad_seguidores()):
        for n in range(len(pt.usuario_cantidad_seguidores())-conteo):
            pdf.text(x = 100, y = (lineas2), txt = str(pt.usuario_cantidad_seguidores()[(lista[indice])]))
            lineas2 = lineas2 +10
            indice = indice + 1
    print (lineas)
    if lineas >110:
        pdf.add_page()
        pdf.text(x = 60, y = 30, txt = 'Análisis '+str(pt.usuario_twitter()))
        pdf.text(x = 10, y = (50), txt = 'Gráfico usuarios con mayor cantidad de seguidores')
        pdf.image("@"+str(pt.usuario_twitter())+" seguidores.png", x = 0, y = 60 , w = 200, h = 150)
        lineas = 60
    else:
        pdf.text(x = 10, y = lineas +20, txt = 'Gráfico usuarios con mayor cantidad de seguidores')
        pdf.image("@"+str(pt.usuario_twitter())+" seguidores.png", x = 0, y = lineas +30 , w = 200, h = 150)

    pdf.add_page()
    pdf.text(x = 60, y = 30, txt = 'Análisis '+str(pt.usuario_twitter()))
    pdf.text(x = 10, y = (50), txt = 'Usuarios con mayor cantidad de retweets')
    lineas = 60
    lineas2 = 60
    lista=[]
    indice = 0
    conteo=0
    for d in pt.conteo_usuario_retweet():
        lista.append(d)
        #print (lista)
        if (lineas == 280):
            for d2 in pt.conteo_usuario_retweet():
                conteo = conteo +1
                if lineas2 ==280:
                    pdf.text(x = 100, y = (lineas2), txt = str(pt.conteo_usuario_retweet()[(lista[indice])]))
                    pdf.text(x = 10, y = (lineas), txt = str(d))
                    lineas = lineas+10
                    #print (indice)
                    lineas2 = 60
                    lista = []
                    indice = 0
                    break
                else:
                    pdf.text(x = 100, y = (lineas2), txt = str(pt.conteo_usuario_retweet()[(lista[indice])]))
                    lineas2 = lineas2 +10
                    indice = indice + 1
                    #print (indice)
                
            pdf.add_page()
            pdf.text(x = 60, y = 30, txt = 'Análisis '+str(pt.usuario_twitter()))
            pdf.text(x = 10, y = (50), txt = 'Usuarios con mayor cantidad de retweets')
            lineas = 60
            
        else:    
            pdf.text(x = 10, y = (lineas), txt = str(d))
            #print(lineas)
            #print(d)
            lineas = lineas +10
    #print (conteo)
    #print(len(d1))
    if conteo < len(pt.conteo_usuario_retweet()):
        for n in range(len(pt.conteo_usuario_retweet())-conteo):
            pdf.text(x = 100, y = (lineas2), txt = str(pt.conteo_usuario_retweet()[(lista[indice])]))
            lineas2 = lineas2 +10
            indice = indice + 1
    print (lineas)
    if lineas >110:
        pdf.add_page()
        pdf.text(x = 60, y = 30, txt = 'Análisis '+str(pt.usuario_twitter()))
        pdf.text(x = 10, y = (50), txt = 'Gráfico Usuarios con mayor cantidad de retweets')
        pdf.image("@"+str(pt.usuario_twitter())+" retweets.png", x = 0, y = 60 , w = 200, h = 150)
        lineas = 60
    else:
        pdf.text(x = 10, y = lineas +20, txt = 'Gráfico Usuarios con mayor cantidad de retweets')
        pdf.image("@"+str(pt.usuario_twitter())+" retweets.png", x = 0, y = lineas +30 , w = 200, h = 150)

    pdf.output(str(pt.usuario_twitter())+"twitter.pdf")



def cerrar_ventana_reporte():
    global ventana_principal
    ventana_principal.deiconify()
    ventana.destroy()
    

def ventana_reporte(ventana_original):
    global ventana_principal
    ventana_principal = ventana_original
    global ventana
    ventana = Tk()
    ventana.title = 'SAD'
    ventana.geometry = '500x450'

    main_frame = Frame(ventana,bg = 'blue')
    main_frame.pack(fill = BOTH, expand = 1)

    my_canvas = Canvas(main_frame, bg = 'blue')
    my_canvas.pack(side = LEFT, fill = BOTH, expand = 1)

    my_scrollbar = ttk.Scrollbar(main_frame, orient = VERTICAL, command = my_canvas.yview)
    my_scrollbar.pack ( side = RIGHT, fill = Y)

    my_canvas.configure(yscrollcommand = my_scrollbar.set )

    my_canvas.bind('<Configure>', lambda e: my_canvas.configure(scrollregion = my_canvas.bbox('all')))

    second_frame = Frame(my_canvas, bg = 'blue')
    second_frame.pack(expand = True, fill = 'both', side = RIGHT)

    my_canvas.create_window((0,0), window = second_frame, anchor = 'nw')

    second_frame = Frame(second_frame, bg = 'red')
    second_frame.pack(expand = True, fill = 'both')


    primera_mitad = Frame(second_frame, bg = "blue")
    primera_mitad.pack( side = LEFT, fill = BOTH, expand = 1)

    segunda_mitad = Frame(second_frame, bg = "blue")
    segunda_mitad.pack(fill = BOTH, expand = 1)

    etiqueta_titulo_fecha =Label(primera_mitad, text = 'Fecha Datos', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha.grid(row = 1, column =0, pady = 5, padx = 10, sticky = W)

    etiqueta_fecha_inicio =Label(primera_mitad, text = 'Fecha inicio :', font = (('Arial'),15))
    etiqueta_fecha_inicio.grid(row = 2, column =0, pady = 5, padx = 10, sticky = W)
    fecha_inicio = Entry(primera_mitad, width = 15, font='Arial 15', justify = CENTER)
    fecha_inicio.insert(0,pt.fecha_fin())
    fecha_inicio.grid(row =2,column = 1)

    etiqueta_fecha_final =Label(primera_mitad, text = 'Fecha termino :', font = (('Arial'),15))
    etiqueta_fecha_final.grid(row = 3, column = 0, pady = 5, padx = 10, sticky = W)
    fecha_final = Entry(primera_mitad, width = 15, font='Arial 15', justify=CENTER)
    fecha_final.insert(0,pt.fecha_inicio())
    fecha_final.grid(row = 3, column = 1, padx = 10)

    boton_retroceso = Button(primera_mitad, text = 'Retroceder', bg = 'green', command = cerrar_ventana_reporte)
    boton_retroceso.grid(row = 0, column = 0, pady = 2, padx = 2, sticky = W)

    etiqueta_titulo_fecha =Label(primera_mitad, text = 'Cantidad de retweet por tweet', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha.grid(row = 4, column = 0, pady = 5)
    espacio = Label(primera_mitad, text ='', bg = 'blue')
    espacio.grid(row = 4, column =1, pady = 5, padx = 10, sticky = W)
    etiqueta_tweet = Label(primera_mitad, text = 'id Tweet :', font = (('Arial'),15))
    etiqueta_tweet.grid(row = 5, column = 0, sticky = W, padx = 10)
    etiqueta_tweet_cantidad = Label(primera_mitad, text = 'Cantidad retweet :', font = (('Arial'),15))
    etiqueta_tweet_cantidad.grid(row = 5, column = 1,sticky=W)
    i = 6
    for cantidad in pt.cantidad_de_retweet_por_tweet_dict():
        tweets = Label(primera_mitad, text = cantidad, font = 'Arial 15')
        tweets.grid(row = i, column =0, pady = 5, padx = 20, sticky = W)
        tweets_cuenta = Label(primera_mitad, text = pt.cantidad_de_retweet_por_tweet_dict()[cantidad], font = 'Arial 15')
        tweets_cuenta.grid(row = i, column =1, pady = 5, padx = 20, sticky = W)
        i = i + 1

    etiqueta_titulo_promedio = Label(primera_mitad, text = 'Promedio Retweet', font = (('Arial'),15, BOLD))
    etiqueta_titulo_promedio.grid(row = i+1, column = 0, pady = 10, padx = 10, sticky=W)
    etiqueta_promedio =Label(primera_mitad, text = 'promedio :', font = (('Arial'),15))
    etiqueta_promedio.grid(row = i+2, column =0, pady = 5, sticky = W, padx = 10)
    promedio = Entry(primera_mitad, width = 5, font='Arial 15', justify=CENTER)
    promedio.insert(0,pt.promedio_retweet())
    promedio.grid(row = i+2, column = 1, pady = 5,sticky = W)

    etiqueta_titulo_fecha_retweet = Label(primera_mitad, text = 'Cantidad de Retweets por fecha', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha_retweet.grid(row = i+3, column = 0,sticky=W, pady = 10, padx = 10)
    i = i+4
    for conteo in pt.conteo_fecha_retweets_dict():
        tweets = Label(primera_mitad, text = conteo, font = 'Arial 15')
        tweets.grid(row = i, column =0, pady = 5, padx = 20, sticky = W)
        tweets_cuenta = Label(primera_mitad, text = pt.conteo_fecha_retweets_dict()[conteo], font = 'Arial 15')
        tweets_cuenta.grid(row = i, column =1, pady = 5, padx = 20, sticky = W)
        i = i + 1
    
    etiqueta_titulo_fecha_retweet = Label(primera_mitad, text = 'usuarios con más seguidores', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha_retweet.grid(row = i+1, column = 0,sticky=W, pady = 10, padx = 10)
    i= i+2
    for conteo in pt.usuario_cantidad_seguidores():
        tweets = Label(primera_mitad, text = conteo, font = 'Arial 15')
        tweets.grid(row = i, column =0, pady = 5, padx = 20, sticky = W)
        tweets_cuenta = Label(primera_mitad, text = pt.usuario_cantidad_seguidores()[conteo], font = 'Arial 15')
        tweets_cuenta.grid(row = i, column =1, pady = 5, padx = 20, sticky = W)
        i = i + 1


    etiqueta_titulo_fecha_retweet = Label(primera_mitad, text = 'usuarios con más retweets', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha_retweet.grid(row = i+3, column = 0,sticky=W, pady = 10, padx = 10)

    i=i+4

    for conteo in pt.conteo_usuario_retweet():
        tweets = Label(primera_mitad, text = conteo, font = 'Arial 15')
        tweets.grid(row = i, column =0, pady = 5, padx = 20, sticky = W)
        tweets_cuenta = Label(primera_mitad, text = pt.conteo_usuario_retweet()[conteo], font = 'Arial 15')
        tweets_cuenta.grid(row = i, column =1, pady = 5, padx = 20, sticky = W)
        i = i + 1
    

    espacio =Label(segunda_mitad, bg='blue')
    espacio.grid(row = 0, column =0, pady = 5, padx = 10, sticky = W)

    grafico1 = plt.Figure(figsize = (15,15), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar2 = FigureCanvasTkAgg(grafico1, segunda_mitad)
    bar2.get_tk_widget().grid(row = 1, column = 0, padx = 5, pady = 10,sticky = W)
    ptc.datos_fecha_tweets().plot(kind='line', legend = True, ax = barras, subplots = True)

    grafico1 = plt.Figure(figsize = (15,20), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar2 = FigureCanvasTkAgg(grafico1,segunda_mitad)
    bar2.get_tk_widget().grid(row = 0, column = 0, padx = 5, pady = 10, sticky = W)
    ptc.datos_cantidad_tweets().plot(kind='bar', legend = True, ax = barras, subplots = True)

    grafico1 = plt.Figure(figsize = (15,12), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar2 = FigureCanvasTkAgg(grafico1,segunda_mitad)
    bar2.get_tk_widget().grid(row = 2, column = 0, padx = 5, pady = 10, sticky = W)
    ptc.datos_usuario_cantidad_seguidores().plot(kind='bar', legend = True, ax = barras, subplots = True)

    grafico1 = plt.Figure(figsize = (15,12), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar2 = FigureCanvasTkAgg(grafico1,segunda_mitad)
    bar2.get_tk_widget().grid(row = 3, column = 0, padx = 5, pady = 10, sticky = W)
    ptc.datos_conteo_usuario_retweet().plot(kind='bar', legend = True, ax = barras, subplots = True)

    etiqueta_titulo_fecha =Label(primera_mitad, text = '', bg="blue")
    etiqueta_titulo_fecha.grid(row = i+1, column =0, pady = 5, padx = 10, sticky = W)
    boton_guardar = Button(primera_mitad, text = 'Guardar en PDF', bg = 'green',fg = 'white', command = guardarpdftwitter)
    boton_guardar.grid(row = i+2, column = 0, pady = 2, padx = 2, sticky = W)






    ventana.mainloop()


