import pandas as pd
from datetime import datetime

usuario = None
usuario2 = None
#usuario ='1367590'
def elegir_usuario(nuevo_usuario):
    global usuario
    usuario = nuevo_usuario
    


def leer_datos():
        data = pd.read_csv(usuario+'.csv')
        df = pd.DataFrame(data)
        return (df)

def fecha_inicio_fin():
    df = leer_datos()
    solo_fecha = pd.to_datetime(df['fecha_tweet']).dt.date
    fecha_inicio = solo_fecha[0]
    fecha_final = solo_fecha[(len(df.fecha_tweet))-1] 
    return ("tweets obtenidos desde {} hasta {}".format(fecha_inicio,fecha_final))

def fecha_inicio():
    df = leer_datos()
    data_original = (df['fecha_review'].value_counts().to_dict())
    fecha = sorted(data_original.items())
    fecha_inicio = fecha[0][0]
    return fecha_inicio

def fecha_fin():
    df = leer_datos()
    data_original = (df['fecha_review'].value_counts().to_dict())
    largo = len(data_original)
    fecha = sorted(data_original.items())
    fecha_fin = fecha[largo-1][0] 
    return fecha_fin


def cantidad_de_reviews():
    df = leer_datos()
    serie = df['id_usuario_review'].value_counts().to_dict()
    for s in serie.values():
        cantidad=s
    return cantidad

def porcentaje_positividad():
    df = leer_datos()
    total = 0
    contador = 0
    positivo = 0
    serie = df['voto'].value_counts().to_dict()
    #print(serie)
    #print(len(serie.keys()))
    if len(serie.keys())==2:
        positivo_negativo = sorted(serie.items())
            #print(serie[contador])
        total = total + serie[contador]
        positivo = positivo_negativo[1][1]
        negativo = positivo_negativo[0][1]
        total = positivo+negativo
        contador = contador + 1
        promedio = positivo/total
        promedio_porcentaje = "{:.2%}".format(promedio)
    else:
        for i  in serie.keys():
            #print(i)
            if(i == True):
                positivo = serie[True]/serie[True]
                promedio_porcentaje = "{:.2%}".format(positivo)
            else:
                negativo = 0
                promedio_porcentaje = "{:.2%}".format(negativo)
    return (promedio_porcentaje)

def idioma_mas_comun():
    df = leer_datos()
    serie = df['idioma'].value_counts().to_dict()
    contador = 0
    total= 0
    #print(serie)
    for t  in serie.keys():
        total = total+serie[t]
    #print (total)
    for i  in serie.keys():
        mas_comun = "{} : {:.2%}".format(i,serie[i]/total)
        break
    return(mas_comun)



#print(fecha_fin(),fecha_inicio())
#print(porcentaje_positividad())

def elegir_usuario2(nuevo_usuario):
    global usuario2
    usuario2 = nuevo_usuario
    


def leer_datos2():
        data = pd.read_csv(usuario2+'.csv')
        df = pd.DataFrame(data)
        return (df)

def fecha_inicio_fin2():
    df = leer_datos2()
    solo_fecha = pd.to_datetime(df['fecha_tweet']).dt.date
    fecha_inicio = solo_fecha[0]
    fecha_final = solo_fecha[(len(df.fecha_tweet))-1] 
    return ("tweets obtenidos desde {} hasta {}".format(fecha_inicio,fecha_final))

def fecha_inicio2():
    df = leer_datos2()
    data_original = (df['fecha_review'].value_counts().to_dict())
    fecha = sorted(data_original.items())
    fecha_inicio = fecha[0][0]
    return fecha_inicio

def fecha_fin2():
    df = leer_datos2()
    data_original = (df['fecha_review'].value_counts().to_dict())
    largo = len(data_original)
    fecha = sorted(data_original.items())
    fecha_fin = fecha[largo-1][0] 
    return fecha_fin


def cantidad_de_reviews2():
    df = leer_datos2()
    serie = df['id_usuario_review'].value_counts().to_dict()
    for s in serie.values():
        cantidad=s
    return cantidad

def porcentaje_positividad2():
    df = leer_datos2()
    total = 0
    contador = 0
    positivo = 0
    serie = df['voto'].value_counts().to_dict()
    #print(serie)
    #print(len(serie.keys()))
    if len(serie.keys())==2:
        positivo_negativo = sorted(serie.items())
            #print(serie[contador])
        total = total + serie[contador]
        positivo = positivo_negativo[1][1]
        negativo = positivo_negativo[0][1]
        total = positivo+negativo
        contador = contador + 1
        promedio = positivo/total
        promedio_porcentaje = "{:.2%}".format(promedio)
    else:
        for i  in serie.keys():
            #print(i)
            if(i == True):
                positivo = serie[True]/serie[True]
                promedio_porcentaje = "{:.2%}".format(positivo)
            else:
                negativo = 0
                promedio_porcentaje = "{:.2%}".format(negativo)
    return (promedio_porcentaje)

def idioma_mas_comun2():
    df = leer_datos2()
    serie = df['idioma'].value_counts().to_dict()
    contador = 0
    total= 0
    #print(serie)
    for t  in serie.keys():
        total = total+serie[t]
    #print (total)
    for i  in serie.keys():
        mas_comun = "{} : {:.2%}".format(i,serie[i]/total)
        break
    return(mas_comun)


