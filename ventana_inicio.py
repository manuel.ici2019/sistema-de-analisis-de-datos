from tkinter import *
from PIL import ImageTk, Image
from tkinter.font import Font
import  ventana_Descarga as vd
import ventana_analisis_twitter as vat
import ventana_analisis_steam as vas
#import ventana as vd2

ventana = None

def cerrar():
    ventana.withdraw()
    vd.ventana_Descargar(ventana)
    #vd2.nueva(ventana)

def cerrar2():
    ventana.withdraw()
    vat.ventana_analisis(ventana)

def cerrar3():
    ventana.withdraw()
    vas.ventana_analisis(ventana)
    
    

ventana = Tk()
ventana.title = "SAD"
ventana.geometry = '600x300+500+500'


primera_mitad = Frame(ventana, bg = "blue")
primera_mitad.pack(expand= True, fill = "both")
label_primera_mitad = Label(primera_mitad, text = "Sistema Análisis de Datos", font = ("Times 30"), bg = "blue", fg = 'white')
label_primera_mitad.grid(padx=130, pady = 40)

segunda_mitad = Frame(ventana, bg = "black")
segunda_mitad.pack(expand= True, fill = "both")
boton_descarga = Button(segunda_mitad, text = "Descargar", bg = "white", command =cerrar, width = 20 )
boton_descarga.grid(row = 0, column = 0, pady = 15, padx = 40)

boton_analizar = Button(segunda_mitad, text = "Analizar Twitter", bg = "white", command = cerrar2, width = 20)
boton_analizar.grid(row = 0, column = 2, pady = 15, padx = 40)

boton_analizar = Button(segunda_mitad, text = "Analizar Steam", bg = "white", command = cerrar3, width = 20)
boton_analizar.grid(row = 0, column = 3, pady = 15, padx = 40)

ventana.mainloop()

