
import pandas as pd
from datetime import datetime
import re
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt 
import seaborn as sns

from pandastweets import usuario_cantidad_seguidores

usuario_publisher = None
busqueda_videojuego = None


def elegir_usuario_publisher(nuevo_usuario):
    global usuario_publisher
    usuario_publisher = nuevo_usuario

def elegir_busqueda_videojuego(usuariojuego1):
    global busqueda_videojuego
    busqueda_videojuego = usuariojuego1

def usuario_publisher_sin_signo(usuario):
    nombre= usuario.replace("@","")
    #print (nombre)
    return(nombre)

def nombre_busqueda():
    global busqueda_videojuego
    return (busqueda_videojuego) 

def formater_usuario_publisher(usuario):
    nombre= usuario.replace(" ","")
    #print (nombre)
    return(nombre)

def leer_datos():
    data = pd.read_csv(usuario_publisher+"publisher.csv")
    df = pd.DataFrame(data)
    return (df)

def fecha_inicio():
    df = leer_datos()
    solo_fecha = pd.to_datetime(df['fecha_tweet']).dt.date
    fecha_inicio = solo_fecha[0] 
    return fecha_inicio

def fecha_fin():
    df = leer_datos()
    solo_fecha = pd.to_datetime(df['fecha_tweet']).dt.date
    fecha_final = solo_fecha[(len(df.fecha_tweet))-1] 
    return fecha_final

def buscarmenciones(variable1, variable2):
    df = leer_datos()
    contador = 0
    contadordf =0
    df["cambio_fecha"] = pd.to_datetime(df['fecha_tweet']).dt.date
    variables =df['c']=df['texto'].str.contains(str(variable1)+'|'+str(variable2),flags=re.IGNORECASE)
    df2 = pd.DataFrame(columns=["fecha_tweet", "id_tweet","texto"])
    for v in variables:
        if v == True:
            #print(v)
            df2.loc[contadordf]=[(df["cambio_fecha"][contador]),df["id_tweet"][contador],df["texto"][contador]]
            #df2["id_tweet"]=[df["id_tweet"][contador]]
            #df2["texto"]=[df["texto"][contador]]
            contadordf=contadordf+1
            
            #print(df2)
        contador=contador+1   
    return(df2)        

def id_menciones(variable1,variable2):
    df = buscarmenciones(variable1, variable2)
    dfserie = df['id_tweet'].value_counts().to_dict
    return dfserie()


def conteo_fecha(variable1,variable2):
    df = buscarmenciones(variable1, variable2)
    dfserie = df['fecha_tweet'].value_counts().to_dict()
    return (dfserie)

def cantidaddemenciones(variable1,variable2):
    df = buscarmenciones(variable1, variable2)
    return (df.shape[0])


def datos_fecha_tweets(variable1, variable2):

    df1 = buscarmenciones(variable1, variable2)
    df1['usuario1'] = df1['id_tweet']
    solo_fecha = pd.to_datetime(df1['fecha_tweet']).dt.date
    df1['fecha_inicio'] = solo_fecha


    df_final = df1.groupby('fecha_inicio')['usuario1'].count()
    df_final.plot(kind='bar', legend = True, subplots = True)
    plt.xticks(rotation=45)
    plt.savefig(str(usuario_publisher)+"publisher.png",bbox_inches = "tight", dpi = 200)
    plt.clf()
    return(df_final)
    
    
#print(fecha_inicio())

#print(buscarmenciones('Tormentedsouls', "Tormented souls"))
#print(cantidaddemenciones('Tormentedsouls', "Tormented souls"))
#formater_usuario_publisher()
#print (nombre_busqueda())
#print(datos_fecha_tweets('Tormentedsouls', "Tormented souls"))
#datos_fecha_tweets("@DualEffectGames")