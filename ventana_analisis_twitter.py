from tkinter import *
from tkinter.font import Font
from PIL import ImageTk, Image
from tkinter import ttk
import ventana_reporte_twitter as vr
import pandastweets as pd
import pandastweetscomparar as pdc
import pandaspublisher as pp
import ventanareportepublisher as vrp



'''mi_imagen = Image.open("iconos/flecha2.png")
escalar = mi_imagen.resize((50,50), Image.ANTIALIAS)
mi_imagen2 = ImageTk.PhotoImage(escalar)'''

'''flecha_retroceso = Button(F_individual, image = mi_imagen2, bg ='blue')
flecha_retroceso.place(relx=0.025, rely=0.025)'''
ventana = None
ventana_principal = None

def cerrar_ventana_analisis():
    global ventana_principal
    ventana_principal.deiconify()
    ventana.destroy()

def abrir_ventana_reporte(usuario_de_twitter):
    global ventana_principal
    ventana.withdraw()
    pd.elegir_usuario_twitter(str(usuario_de_twitter.get()))
    pdc.elegir_usuario_twitter(str(usuario_de_twitter.get()))
    vr.ventana_reporte(ventana)
    
def abrir_ventana_reporte_publisher(usuario_editor,usuario_juego1):
    global ventana_principal
    ventana.withdraw()
    pp.elegir_usuario_publisher(str(usuario_editor.get()))
    pp.elegir_busqueda_videojuego(str(usuario_juego1.get()))
    vrp.ventana_reporte(ventana)
      


def ventana_analisis(ventana_original):

    global ventana_principal
    ventana_principal = ventana_original
    global ventana
    ventana = Tk()
    ventana.title = "SAD"
    ventana.geometry = "600x300+500+500"


    

    F_individual = Frame(ventana, bg = "blue" )
    F_individual.pack(expand = True , fill = 'both')

    boton_retroceso = Button(F_individual, text = 'retroceder', bg = 'green', command = cerrar_ventana_analisis)
    boton_retroceso.grid( row = 0, column = 0, pady = 5, sticky = W, padx = 5)

    etiqueta_titulo = Label(F_individual, text = "Nombre Usuario Twitter", font = 'Arial 12', fg = 'white', bg = 'blue')
    etiqueta_titulo.grid(row = 1, column = 0, padx = 150, pady = 5)

    usuario_twitter = StringVar()
    usuario_twitter_combobox = ttk.Combobox(F_individual, textvariable = usuario_twitter, values = (['@DualEffectGames','@RebeccaRE13','@InvaderDevs']), width=30)
    usuario_twitter_combobox.grid(row = 2, column = 0, padx = 150, pady = 5)

    Boton_individual = Button(F_individual, text = 'Analizar', bg = "white", command = lambda: abrir_ventana_reporte(usuario_twitter_combobox))
    Boton_individual.grid(row = 3, column = 0, padx = 150, pady = 20)
   


    F_editor = Frame(ventana, bg = "red" )
    F_editor.pack(expand = True , fill = 'both')

    etiqueta_editor = Label(F_editor, text = "Nombre Publisher", font = 'Arial 12', fg = 'white', bg = 'red')
    etiqueta_editor.grid(row = 1, column = 1, padx = 150, pady = 5)

    etiqueta_juego1 = Label(F_editor, text = "videojuego buscado", font = 'Arial 12', fg = 'white', bg = 'red')
    etiqueta_juego1.grid(row = 3, column = 1, padx = 150, pady = 5)

    usuario_editor = StringVar()
    usuario_editor_combobox = ttk.Combobox(F_editor, textvariable = usuario_editor, values = (['@DualEffectGames','@RebeccaRE13','@InvaderDevs']), width=30)
    usuario_editor_combobox.grid(row = 2, column = 1,padx = 150, pady = 5)

    usuario_juego1 = StringVar()
    usuario_juego1_combox = ttk.Combobox(F_editor, textvariable = usuario_juego1, values = (['@DualEffectGames','@RebeccaRE13','@InvaderDevs']), width=30)
    usuario_juego1_combox.grid(row = 4 , column = 1,padx = 150, pady = 5)


    Boton_editor = Button(F_editor, text = 'Analizar', bg = "white",command = lambda: abrir_ventana_reporte_publisher(usuario_editor_combobox,usuario_juego1_combox))
    Boton_editor.grid(row = 5, column = 1, padx = 150, pady = 20)



    ventana.mainloop()

 