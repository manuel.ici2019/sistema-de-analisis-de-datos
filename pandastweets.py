import pandas as pd
from datetime import datetime

usuario_de_twitter = None

def elegir_usuario_twitter(nuevo_usuario):
    global usuario_de_twitter
    usuario_de_twitter = nuevo_usuario

def leer_datos():
    data = pd.read_csv(usuario_de_twitter+".csv")
    df = pd.DataFrame(data)
    return (df)

def fecha_inicio_fin():
    df = leer_datos()
    solo_fecha = pd.to_datetime(df['fecha_tweet']).dt.date
    fecha_inicio = solo_fecha[0]
    fecha_final = solo_fecha[(len(df.fecha_tweet))-1] 
    return ("tweets obtenidos desde {} hasta {}".format(fecha_inicio,fecha_final))

def fecha_inicio():
    df = leer_datos()
    solo_fecha = pd.to_datetime(df['fecha_tweet']).dt.date
    fecha_inicio = solo_fecha[0] 
    return fecha_inicio

def fecha_fin():
    df = leer_datos()
    solo_fecha = pd.to_datetime(df['fecha_tweet']).dt.date
    fecha_final = solo_fecha[(len(df.fecha_tweet))-1] 
    return fecha_final


def cantidad_de_retweet_por_tweet():
    df = leer_datos()
    serie = df['id_tweet'].value_counts()
    return serie

def cantidad_de_retweet_por_tweet_dict():
    df = leer_datos()
    dfserie = df['id_tweet'].value_counts().to_dict
    return dfserie()

def promedio_retweet():
    total = 0
    contador = 0
    for i  in cantidad_de_retweet_por_tweet():
        contador = contador + 1
        #print (contador)
        #print (i)
        total = total +i
    promedio = total/contador
    return (promedio)

def conteo_lugares():
    df = leer_datos()
    localizar = df['localizacion'].value_counts()
    return localizar

def conteo_lugares_html():
    df = leer_datos()
    localizar = df['localizacion'].value_counts()
    localizar_html =pd.DataFrame(localizar).to_html
    return (localizar_html())

def conteo_fecha_retweets():
    df = leer_datos()
    solo_fecha = pd.to_datetime(df['fecha_retweet']).dt.date
    fecha_YMD = []
    for fecha in solo_fecha:
        fecha_YMD.append(str(fecha))
    df.insert(5,'Fecha',fecha_YMD,True)
    cuenta_fecha = df['Fecha'].value_counts()
    return cuenta_fecha

        

def conteo_fecha_retweets_dict():
    df = leer_datos()
    solo_fecha = pd.to_datetime(df['fecha_retweet']).dt.date
    fecha_YMD = []
    for fecha in solo_fecha:
        fecha_YMD.append(str(fecha))
    df.insert(5,'Fecha',fecha_YMD,True)
    cuenta_fecha = df['Fecha'].value_counts().to_dict
    return cuenta_fecha()

def conteo_usuario_retweet():
    df = leer_datos()
    usuario= df['usuario_retweet'].value_counts().head(15).to_dict()
    return usuario

def usuario_cantidad_seguidores():
    df = leer_datos()
    usuario= df.drop_duplicates('usuario_retweet')
    tabla_final = usuario[['usuario_retweet','followers']].sort_values(by = 'followers',ascending=[False]).head(15)
    diccionario = dict(zip(tabla_final['usuario_retweet'],tabla_final['followers']))
    return(diccionario)

def usuario_twitter():
    df = leer_datos()
    solo_usuario = df["usuario_tweet"].value_counts().to_dict()
    for n in solo_usuario.keys():
        pass
    return (n)
