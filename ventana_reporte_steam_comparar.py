from tkinter import *
from tkinter import ttk
import pandas_review as pr
from tkinter.font import BOLD
import pandas_review_acumulado as pra
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plt 
from ventana_analisis_steam import*
from fpdf import FPDF

 
ventana = None
ventana_principal = None

def guardarcompararpdf():
    pdf = FPDF(orientation='P', unit = 'mm', format = 'A4')
    pdf.add_page()
    pdf.set_font("Arial",'B',20)
    pdf.text(x = 35, y = 30, txt = 'Análisis '+str(pra.usuario_nombre_formateado())+"-"+ str(pra.usuario_nombre_formateado2()))
    pdf.set_font("Arial",'',16)
    pdf.text(x = 10, y = 60, txt = 'Videojuego: '+ str(pra.usuario_nombre_formateado()))
    pdf.text(x = 110, y = 60, txt = 'Videojuego: '+ str(pra.usuario_nombre_formateado2()))
    pdf.text(x = 10, y = 70, txt = 'fecha inicio: '+ str(pr.fecha_inicio()))
    pdf.text(x = 110, y = 70, txt = 'fecha inicio: '+ str(pr.fecha_inicio2()))
    pdf.text(x = 10, y = 80, txt = 'fecha Termino: '+ str(pr.fecha_fin()))
    pdf.text(x = 110, y = 80, txt = 'fecha Termino: '+ str(pr.fecha_fin2()))
    pdf.text(x = 10, y = 90, txt = 'Cantidad Review: '+ str(pr.cantidad_de_reviews()))
    pdf.text(x = 110, y = 90, txt = 'Cantidad Review: '+ str(pr.cantidad_de_reviews2()))
    pdf.text(x = 10, y = 100, txt = 'Positividad : '+ str(pr.porcentaje_positividad()))
    pdf.text(x = 110, y = 100, txt = 'Positividad : '+ str(pr.porcentaje_positividad2()))
    pdf.text(x = 10, y = 110, txt = 'Idioma más comun : '+ str(pr.idioma_mas_comun()))
    pdf.text(x = 110, y = 110, txt = 'Idioma más comun : '+ str(pr.idioma_mas_comun2()))
    pdf.text(x = 10, y = 130, txt = 'Ventas acumuladas')
    pdf.image(str(pra.usuario_nombre_formateado())+" ventas.png", x = 0, y = 140, w = 100, h = 130)
    pdf.image(str(pra.usuario_nombre_formateado2())+" ventas.png", x = 100, y = 140, w = 100, h = 130)
    pdf.add_page()
    pdf.set_font("Arial",'B',20)
    pdf.text(x = 35, y = 30, txt = 'Análisis '+str(pra.usuario_nombre_formateado())+"-"+ str(pra.usuario_nombre_formateado2()))
    pdf.set_font("Arial",'',16)
    pdf.text(x = 10, y = 60, txt = 'Cantidad de Review durante el tiempo')
    pdf.image(str(pra.usuario_nombre_formateado())+" review.png", x = 0, y = 70, w = 100, h = 130)
    pdf.image(str(pra.usuario_nombre_formateado2())+" review.png", x = 100, y = 70, w = 100, h = 130)
    pdf.add_page()
    pdf.set_font("Arial",'B',20)
    pdf.text(x = 35, y = 30, txt = 'Análisis '+str(pra.usuario_nombre_formateado())+"-"+ str(pra.usuario_nombre_formateado2()))
    pdf.set_font("Arial",'',16)
    pdf.text(x = 10, y = 60, txt = 'Cantidad de Review  negativas durante el tiempo')
    pdf.image(str(pra.usuario_nombre_formateado())+" negativo.png", x = 0, y = 70, w = 100, h = 130)
    pdf.image(str(pra.usuario_nombre_formateado2())+" negativo.png", x = 100, y = 70, w = 100, h = 130)
    pdf.output(str(pra.usuario_nombre_formateado())+"-"+str(pra.usuario_nombre_formateado2()) +"steam.pdf")


def cerrar_ventana_reporte():
    global ventana_principal
    ventana_principal.deiconify()
    ventana.destroy()
    

def ventana_reporte(ventana_original):
    global ventana_principal
    ventana_principal = ventana_original
    global ventana
    ventana = Tk()
    ventana.title = 'SAD'
    ventana.geometry = '500x450'

    main_frame = Frame(ventana,bg = 'blue')
    main_frame.pack(fill = BOTH, expand = 1)

    my_canvas = Canvas(main_frame, bg = 'red')
    my_canvas.pack(side = LEFT, fill = BOTH, expand = 1)

    my_scrollbar = ttk.Scrollbar(main_frame, orient = VERTICAL, command = my_canvas.yview)
    my_scrollbar.pack ( side = RIGHT, fill = Y)

    my_canvas.configure(yscrollcommand = my_scrollbar.set )

    my_canvas.bind('<Configure>', lambda e: my_canvas.configure(scrollregion = my_canvas.bbox('all')))

    second_frame = Frame(my_canvas, bg = 'blue')
    second_frame.pack(expand = True, fill = 'both', side = RIGHT)

    my_canvas.create_window((0,0), window = second_frame, anchor = 'nw')

    second_frame = Frame(second_frame, bg = 'red')
    second_frame.pack(expand = True, fill = 'both')


    primera_mitad = Frame(second_frame, bg = "blue")
    primera_mitad.pack( side = LEFT, fill = BOTH, expand = 1)

    boton_retroceso = Button(primera_mitad, text = 'Retroceder', bg = 'green', command = cerrar_ventana_reporte)
    boton_retroceso.grid(row = 0, column = 0, pady = 2, padx = 2, sticky = W)

    etiqueta_titulo_fecha =Label(primera_mitad, text = 'Fecha Datos', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha.grid(row = 1, column =0, pady = 5, padx = 10, sticky = W)


    etiqueta_fecha_inicio =Label(primera_mitad, text = 'Fecha inicio :', font = (('Arial'),15))
    etiqueta_fecha_inicio.grid(row = 2, column =0, pady = 5, padx = 10, sticky = W)
    fecha_inicio = Entry(primera_mitad, width = 15, font='Arial 15', justify = CENTER)
    fecha_inicio.insert(0,pr.fecha_inicio())
    fecha_inicio.grid(row =2,column = 1)

    etiqueta_fecha_final =Label(primera_mitad, text = 'Fecha termino :', font = (('Arial'),15))
    etiqueta_fecha_final.grid(row = 3, column = 0, pady = 5, padx = 10, sticky = W)
    fecha_final = Entry(primera_mitad, width = 15, font='Arial 15', justify=CENTER)
    fecha_final.insert(0,pr.fecha_fin())
    fecha_final.grid(row = 3, column = 1, padx = 10)

    etiqueta_nombre = Label(primera_mitad, text = 'Videojuego:', font = (('Arial'),15))
    etiqueta_nombre.grid(row = 4, column = 0, pady = 5, padx = 10, sticky = W)
    nombre = Entry(primera_mitad, width = 15, font='Arial 15', justify=CENTER)
    nombre.insert(0,pra.usuario_nombre_formateado())
    nombre.grid(row = 4, column = 1, padx = 10)

    etiqueta_cantidad_review = Label(primera_mitad, text = 'Cantidad de Review :', font = (('Arial'),15))
    etiqueta_cantidad_review.grid(row = 5, column = 0, pady = 5, padx = 10, sticky = W)
    cantidad_review = Entry(primera_mitad, width = 15, font='Arial 15', justify=CENTER)
    cantidad_review.insert(0,pr.cantidad_de_reviews())
    cantidad_review.grid(row = 5, column = 1, padx = 10)

    etiqueta_positividad = Label(primera_mitad, text = 'Porcentaje de positividad :', font = (('Arial'),15))
    etiqueta_positividad.grid(row = 6,column = 0 , pady =5, padx = 10,sticky = W)
    positividad = Entry(primera_mitad, width = 15, font='Arial 15', justify=CENTER)
    positividad.insert(0,pr.porcentaje_positividad())
    positividad.grid(row =6,column = 1, padx = 10)

    etiqueta_idioma = Label(primera_mitad, text = 'Idioma más comun :', font = (('Arial'),15))
    etiqueta_idioma.grid(row = 7,column = 0 , pady =5, padx = 10,sticky = W)
    idioma = Entry(primera_mitad, width = 15, font='Arial 15', justify=CENTER)
    idioma.insert(0,pr.idioma_mas_comun())
    idioma.grid(row =7,column = 1, padx = 10)

    etiqueta_grafico_ventas = Label(primera_mitad, text = 'Ventas acumuladas :', font = (('Arial'),15))
    etiqueta_grafico_ventas.grid(row = 8,column = 0 , pady =5, padx = 10,sticky = W)
    grafico1 = plt.Figure(figsize = (9,6), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar1 = FigureCanvasTkAgg(grafico1, primera_mitad)
    bar1.get_tk_widget().grid(row = 9, column = 0, padx = 5, pady = 10)
    apisteam = pra.llamada_API()
    pra.datos_grafico(apisteam).plot(kind='line', legend = True, ax = barras, subplots = True)

    etiqueta_grafico_cantidad_review = Label(primera_mitad, text = 'Cantidad de review durante el tiempo :', font = (('Arial'),15))
    etiqueta_grafico_cantidad_review.grid(row = 10,column = 0 , pady =5, padx = 10,sticky = W)
    grafico1 = plt.Figure(figsize = (9,6), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar2 = FigureCanvasTkAgg(grafico1, primera_mitad)
    bar2.get_tk_widget().grid(row = 11, column = 0, padx = 5, pady = 10)
    pra.datos_steam2().plot(kind='line', legend = True, ax = barras, subplots = True)

    etiqueta_grafico_cantidad_negativa = Label(primera_mitad, text = 'Cantidad de review negativas durante el tiempo :', font = (('Arial'),15))
    etiqueta_grafico_cantidad_negativa.grid(row = 12,column = 0 , pady =5, padx = 10,sticky = W)
    grafico1 = plt.Figure(figsize = (9,6), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar2 = FigureCanvasTkAgg(grafico1, primera_mitad)
    bar2.get_tk_widget().grid(row = 13, column = 0, padx = 5, pady = 10)
    pra.datos_steam_negativos().plot(kind='line', legend = True, ax = barras, subplots = True, color = 'red')


    segunda_mitad = Frame(second_frame, bg = "red")
    segunda_mitad.pack(fill = BOTH, expand = 1)

    espacio =Label(segunda_mitad, bg = 'red')
    espacio.grid(row = 0, column =0, pady = 5, padx = 10, sticky = W)

    etiqueta_titulo_fecha =Label(segunda_mitad, text = 'Fecha Datos', font = (('Arial'),15, BOLD))
    etiqueta_titulo_fecha.grid(row = 1, column =0, pady = 5, padx = 10, sticky = W)


    etiqueta_fecha_inicio =Label(segunda_mitad, text = 'Fecha inicio :', font = (('Arial'),15))
    etiqueta_fecha_inicio.grid(row = 2, column =0, pady = 5, padx = 10, sticky = W)
    fecha_inicio = Entry(segunda_mitad, width = 15, font='Arial 15', justify = CENTER)
    fecha_inicio.insert(0,pr.fecha_inicio2())
    fecha_inicio.grid(row =2,column = 1)

    etiqueta_fecha_final =Label(segunda_mitad, text = 'Fecha termino :', font = (('Arial'),15))
    etiqueta_fecha_final.grid(row = 3, column = 0, pady = 5, padx = 10, sticky = W)
    fecha_final = Entry(segunda_mitad, width = 15, font='Arial 15', justify=CENTER)
    fecha_final.insert(0,pr.fecha_fin2())
    fecha_final.grid(row = 3, column = 1, padx = 10)

    etiqueta_nombre = Label(segunda_mitad, text = 'Videojuego:', font = (('Arial'),15))
    etiqueta_nombre.grid(row = 4, column = 0, pady = 5, padx = 10, sticky = W)
    nombre = Entry(segunda_mitad, width = 15, font='Arial 15', justify=CENTER)
    nombre.insert(0,pra.usuario_nombre_formateado2())
    nombre.grid(row = 4, column = 1, padx = 10)

    etiqueta_cantidad_review = Label(segunda_mitad, text = 'Cantidad de Review :', font = (('Arial'),15))
    etiqueta_cantidad_review.grid(row = 5, column = 0, pady = 5, padx = 10, sticky = W)
    cantidad_review = Entry(segunda_mitad, width = 15, font='Arial 15', justify=CENTER)
    cantidad_review.insert(0,pr.cantidad_de_reviews2())
    cantidad_review.grid(row = 5, column = 1, padx = 10)

    etiqueta_positividad = Label(segunda_mitad, text = 'Porcentaje de positividad :', font = (('Arial'),15))
    etiqueta_positividad.grid(row = 6,column = 0 , pady =5, padx = 10,sticky = W)
    positividad = Entry(segunda_mitad, width = 15, font='Arial 15', justify=CENTER)
    positividad.insert(0,pr.porcentaje_positividad2())
    positividad.grid(row =6,column = 1, padx = 10)

    etiqueta_idioma = Label(segunda_mitad, text = 'Idioma más comun :', font = (('Arial'),15))
    etiqueta_idioma.grid(row = 7,column = 0 , pady =5, padx = 10,sticky = W)
    idioma = Entry(segunda_mitad, width = 15, font='Arial 15', justify=CENTER)
    idioma.insert(0,pr.idioma_mas_comun2())
    idioma.grid(row =7,column = 1, padx = 10)

    etiqueta_grafico_ventas = Label(segunda_mitad, text = 'Ventas acumuladas :', font = (('Arial'),15))
    etiqueta_grafico_ventas.grid(row = 8,column = 0 , pady =5, padx = 10,sticky = W)
    grafico1 = plt.Figure(figsize = (9,6), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar1 = FigureCanvasTkAgg(grafico1, segunda_mitad)
    bar1.get_tk_widget().grid(row = 9, column = 0, padx = 5, pady = 10)
    apisteam = pra.llamada_API()
    pra.datos_grafico2(apisteam).plot(kind='line', legend = True, ax = barras, subplots = True)


    etiqueta_grafico_cantidad_review = Label(segunda_mitad, text = 'Cantidad de review durante el tiempo :', font = (('Arial'),15))
    etiqueta_grafico_cantidad_review.grid(row = 10,column = 0 , pady =5, padx = 10,sticky = W)
    grafico1 = plt.Figure(figsize = (9,6), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar2 = FigureCanvasTkAgg(grafico1, segunda_mitad)
    bar2.get_tk_widget().grid(row = 11, column = 0, padx = 5, pady = 10)
    pra.datos_steam22().plot(kind='line', legend = True, ax = barras, subplots = True)

    etiqueta_grafico_cantidad_negativa = Label(segunda_mitad, text = 'Cantidad de review negativas durante el tiempo :', font = (('Arial'),15))
    etiqueta_grafico_cantidad_negativa.grid(row = 12,column = 0 , pady =5, padx = 10,sticky = W)
    grafico1 = plt.Figure(figsize = (9,6), dpi = 60)
    barras = grafico1.add_subplot(111)
    bar2 = FigureCanvasTkAgg(grafico1, segunda_mitad)
    bar2.get_tk_widget().grid(row = 13, column = 0, padx = 5, pady = 10)
    pra.datos_steam_negativos2().plot(kind='line', legend = True, ax = barras, subplots = True, color = 'red')
   

    boton_guardar = Button(primera_mitad, text = 'Guardar en PDF', bg = 'green',fg = 'white', command = guardarcompararpdf)
    boton_guardar.grid(row = 14, column = 0, pady = 2, padx = 2, sticky = W)


    ventana.mainloop()


