import tweepy
import csv
import credenciales
import json
import urllib
import re 
from re import split
import datostweet as dt


#Autenticar llaves
auth = tweepy.OAuthHandler(credenciales.API_KEY,credenciales.API_SECRET_KEY)
auth.set_access_token(credenciales.ACCESS_TOKEN,credenciales.ACCESS_TOKEN_SECRET)

#ingresar a la API
api = tweepy.API(auth)


# fetching the statuses
def tweets_historicos(nombre_de_usuario):
    nombre = nombre_de_usuario
    statuses = api.user_timeline(screen_name = nombre, exclude_replies = True, include_rts = False, count = 100)
    id = []
    salida_csv =[]
    for tweet in statuses:
        id.append([tweet.created_at, tweet.id_str, dt.strip_undesired_chars(tweet.text)])
        #salida_csv = salida_csv + id
    return(id)
    #return (statuses)

def exportar_csv_publisher(nombre_de_usuario):
    nombre = nombre_de_usuario
    with open(nombre + 'publisher.csv', "w", newline='', encoding = "utf-8") as f:       
                writer = csv.writer(f, quoting=csv.QUOTE_ALL)
                writer.writerow(['fecha_tweet','id_tweet','texto'])
                writer.writerows(tweets_historicos(nombre))   


#exportar_csv_publisher('@DualEffectGames')
#print(tweets_historicos('@DualEffectGames'))